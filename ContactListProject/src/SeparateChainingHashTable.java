import java.util.LinkedList;
import java.util.List;

public class SeparateChainingHashTable<E>
{
    public SeparateChainingHashTable( )
    {
        this( DEFAULT_TABLE_SIZE );
    }

    public SeparateChainingHashTable( int initialSize )
    {
        nameSize = 0;
        numberSize = 0;
    	nameTable = new LinkedList[ nextPrime( initialSize ) ];
        numberTable = new LinkedList[ nextPrime( initialSize ) ];
        for( int i = 0; i < nameTable.length; i++ )
            nameTable[ i ] = new LinkedList<>( );
        for( int i = 0; i < numberTable.length; i++ )
            numberTable[ i ] = new LinkedList<>( );
    }
    
    public String formatNumber( String number )
	{
		StringBuilder sb = new StringBuilder( number );
		for( int i = 0; i < sb.length(); i++ )
		{
			if( !Character.isDigit( sb.charAt( i ) ) )
				sb.deleteCharAt( i );
		}
		return sb.toString();
	}
    
    public boolean isNumber( String nameORnumber )
	{
		StringBuilder sb = new StringBuilder( nameORnumber );
		for( int i = 0; i < sb.length(); i++ )
		{
			if( Character.isDigit( sb.charAt( i ) ) )
				return true;
		}
		return false;
	}
    
    public boolean insert(String name, String number)
    {
    	List<Contact> whichNameList = nameTable[ hash( name, nameSize ) ];
    	List<Contact> whichNumberList = numberTable[ hash( number, numberSize ) ];
    	boolean addedName = false;
    	boolean addedNumber = false;
    	Contact newContact = new Contact( name, number );
        if( !whichNameList.contains( newContact ) )
        {
        	if( whichNameList.add( newContact ) )
        	{
        		addedName = true;
        		nameSize++;
        	}
        }
        if( !whichNumberList.contains( newContact ) )
        {
            if( whichNumberList.add( newContact ) )
            {
            	addedNumber = true;
            	numberSize++;
            }
        }
        if( nameSize > nameTable.length || numberSize > numberTable.length )
    		rehash();
        return addedName && addedNumber;
    }

    public boolean delete( String nameORnumber )
    {
    	if( !isNumber( nameORnumber ) )
    		return deleteWithName( nameORnumber );
    	if( isNumber( nameORnumber ) )
        	return deleteWithNumber( formatNumber( nameORnumber ) );
    	return false;
    }
    
    private boolean deleteWithName( String name )
    {
    	List<Contact> whichList = nameTable[ hash( name, nameSize ) ];
    	String numberToBeDeleted;
    	for( Contact currentContact: whichList )
    	{
    		if( currentContact.name.equals( name ) )
    		{
    			numberToBeDeleted = currentContact.number;
    			whichList.remove( currentContact );
    			nameSize--;
    			deleteWithNumber( numberToBeDeleted );
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    private boolean deleteWithNumber( String number )
    {
    	List<Contact> whichList = nameTable[ hash( number, numberSize ) ];
    	String nameToBeDeleted;
    	for( Contact currentContact: whichList )
    	{
    		if( currentContact.number.equals( number ) )
    		{
    			nameToBeDeleted = currentContact.name;
    			whichList.remove( currentContact );
    			numberSize--;
    			deleteWithName( nameToBeDeleted );
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    public String find(String nameORnumber)
    {
    	if( !isNumber( nameORnumber ) )
    		return findName( nameORnumber );
    	if( isNumber( nameORnumber ) )
    		return findNumber( formatNumber( nameORnumber ) );
    	return null;
    }
    
    public String findName( String name )
    {
    	List<Contact> whichList = nameTable[ hash( name, nameSize ) ];
    	for( Contact currentContact: whichList )
    	{
    		if( currentContact.name.equals( name ) )
    			return currentContact.number;
    	}
    	return null;
    }
    
    public String findNumber( String number )
    {
    	List<Contact> whichList = numberTable[ hash( number, numberSize ) ];
    	for( Contact currentContact: whichList )
    	{
    		if( currentContact.number.equals( number ) )
    			return currentContact.name;
    	}
    	return null;
    }
    
    public int size()
    {
    return nameSize;	
    }
    
    public void printAllContacts()
    {
    	
    }

    public static int hash( String key, int tableSize )
    {
        int hashVal = 0;

        for( int i = 0; i < key.length( ); i++ )
            hashVal = 37 * hashVal + key.charAt( i );

        hashVal %= tableSize;
        if( hashVal < 0 )
            hashVal += tableSize;

        return hashVal;
    }

    private void rehash()
    {
        List<Contact>[]  oldName = nameTable;
        List<Contact>[] oldNumber = numberTable;

            // Create new double-sized, empty table
        nameTable = new LinkedList[ nextPrime( 2 * oldName.length ) ];
        numberTable = new LinkedList[ nextPrime( 2 * oldNumber.length ) ];
        for( int i = 0; i < nameTable.length; i++ )
            nameTable[ i ] = new LinkedList<>();
        for( int i = 0; i < numberTable.length; i++ )
        	numberTable[ i ] = new LinkedList<>();

            // Copy table over
        nameSize = 0;
        numberSize = 0;
        for( List<Contact> currentList : oldName )
            for( Contact item : currentList )
                insert( item.name, item.number );
    }
    
    private static class Contact
    {
    	Contact( String theName, String theNumber )
    	{
    		name = theName;
    		number = theNumber;
    	}
    	private String name;
    	private String number;
    }

    private static int nextPrime( int n )
    {
        if( n % 2 == 0 )
            n++;

        for( ; !isPrime( n ); n += 2 )
            ;

        return n;
    }

    private static boolean isPrime( int n )
    {
        if( n == 2 || n == 3 )
            return true;

        if( n == 1 || n % 2 == 0 )
            return false;

        for( int i = 3; i * i <= n; i += 2 )
            if( n % i == 0 )
                return false;

        return true;
    }
    
    private static final int DEFAULT_TABLE_SIZE = 101;
    private List<Contact>[] nameTable;
    private List<Contact>[] numberTable;
    private int nameSize;
    private int numberSize;
}