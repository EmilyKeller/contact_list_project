import java.util.HashMap;

public class ContactListHashMap
{
	public ContactListHashMap()
	{
		nameTable = new HashMap<String, String>();
		numberTable = new HashMap<String, String>();
	}

	public String formatNumber( String number )
	{
		StringBuilder sb = new StringBuilder( number );
		for( int i = 0; i < sb.length(); i++ )
		{
			if( !Character.isDigit( sb.charAt( i ) ) )
				sb.deleteCharAt( i );
		}
		return sb.toString();
	}
	
	public boolean isNumber( String nameORnumber )
	{
		StringBuilder sb = new StringBuilder( nameORnumber );
		for( int i = 0; i < sb.length(); i++ )
		{
			if( Character.isDigit( sb.charAt( i ) ) )
				return true;
		}
		return false;
	}
	
	public boolean insert( String name, String number ) // no duplicates
	{
		number = formatNumber( number );
		if( nameTable.putIfAbsent( name, number ) == null 
				&& numberTable.putIfAbsent( number,  name ) == null )
			return true;
		return false;
	}
	
	public String find( String nameORnumber )
	{
		if( !isNumber( nameORnumber ) )
			return findWithName( nameORnumber );
		if( isNumber( nameORnumber ) )
			return findWithNumber( formatNumber( nameORnumber ) );
		return null;
	}

	public String findWithName( String name )
	{
		return nameTable.get( name );
	}
	
	public String findWithNumber( String number )
	{
		return numberTable.get( number );
	}
	
	public boolean delete(String nameORnumber)
	{
		if( !isNumber( nameORnumber ) )
			return deleteWithName( nameORnumber );
		if( isNumber( nameORnumber ) )
			return deleteWithNumber( formatNumber( nameORnumber ) );
		return false;
	}
	
	public boolean deleteWithName( String name )
	{
		String numberToBeRemoved = nameTable.remove( name );
		if( numberToBeRemoved != null )
			if( numberTable.remove( numberToBeRemoved ) != null )
				return true;
		return false;
	}
	
	public boolean deleteWithNumber( String number )
	{
		String nameToBeRemoved = numberTable.remove( number );
		if( nameToBeRemoved != null )
			if( nameTable.remove( nameToBeRemoved ) != null )
				return true;
		return false;
	}
	
	public int size()
	{
		return nameTable.size();
	}
	
	public void printAllContacts()
	{
		for (String key : nameTable.keySet())
		{
		   System.out.print( "Name: " + key + "; Number: " );
		   System.out.println( nameTable.get( key ) );
		}
	}
	
	private HashMap<String, String> nameTable;
	private HashMap<String, String> numberTable;
}
